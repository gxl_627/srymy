package com.srymy.gb28181.part3;

import com.srymy.gb28181.part1.Part3Application;
import com.srymy.gb28181.part1.service.CatalogService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import javax.sip.InvalidArgumentException;
import javax.sip.SipException;
import java.text.ParseException;

@SpringBootTest(classes = { Part3Application.class})//配置启动类
class Part1ApplicationTests {

    @Autowired
    private CatalogService catalogService;

    @Test
    public void testInsertJob() {
        try {
            catalogService.sendCatalog();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (SipException e) {
            e.printStackTrace();
        } catch (InvalidArgumentException e) {
            e.printStackTrace();
        }
    }

}
